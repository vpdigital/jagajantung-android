// Generated code from Butter Knife. Do not modify!
package com.ultimate.camera.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class NativeCameraFragment$$ViewInjector {
  public static void inject(Finder finder, final com.ultimate.camera.fragments.NativeCameraFragment target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492891, "field 'tv_rate'");
    target.tv_rate = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492893, "field 'tv_action'");
    target.tv_action = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492890, "field 'tv_guide'");
    target.tv_guide = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492881, "field 'btn_tnc'");
    target.btn_tnc = (android.widget.Button) view;
  }

  public static void reset(com.ultimate.camera.fragments.NativeCameraFragment target) {
    target.tv_rate = null;
    target.tv_action = null;
    target.tv_guide = null;
    target.btn_tnc = null;
  }
}
