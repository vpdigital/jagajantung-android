// Generated code from Butter Knife. Do not modify!
package com.raay.jagajantung.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class ResultFragment$$ViewInjector {
  public static void inject(Finder finder, final com.raay.jagajantung.fragments.ResultFragment target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492914, "field 'tv_heart'");
    target.tv_heart = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492920, "field 'tv_desc'");
    target.tv_desc = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492881, "field 'btn_tnc'");
    target.btn_tnc = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492923, "field 'btn_next'");
    target.btn_next = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492922, "field 'btn_save'");
    target.btn_save = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492916, "field 'rgCondition'");
    target.rgCondition = (android.widget.RadioGroup) view;
    view = finder.findRequiredView(source, 2131492921, "field 'btn_back'");
    target.btn_back = (android.widget.Button) view;
  }

  public static void reset(com.raay.jagajantung.fragments.ResultFragment target) {
    target.tv_heart = null;
    target.tv_desc = null;
    target.btn_tnc = null;
    target.btn_next = null;
    target.btn_save = null;
    target.rgCondition = null;
    target.btn_back = null;
  }
}
