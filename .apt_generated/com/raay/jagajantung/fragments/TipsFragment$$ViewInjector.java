// Generated code from Butter Knife. Do not modify!
package com.raay.jagajantung.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class TipsFragment$$ViewInjector {
  public static void inject(Finder finder, final com.raay.jagajantung.fragments.TipsFragment target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492881, "field 'btn_tnc'");
    target.btn_tnc = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492940, "field 'tv_exer'");
    target.tv_exer = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492937, "field 'tv_avoid'");
    target.tv_avoid = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492941, "field 'tv_exer_d'");
    target.tv_exer_d = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492931, "field 'tv_kolesterol'");
    target.tv_kolesterol = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492947, "field 'tv_smoke_d'");
    target.tv_smoke_d = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492929, "field 'tv_eat_d'");
    target.tv_eat_d = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492946, "field 'tv_smoke'");
    target.tv_smoke = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492928, "field 'tv_eat'");
    target.tv_eat = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492925, "field 'tv_h'");
    target.tv_h = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492926, "field 'tv_h_d'");
    target.tv_h_d = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492932, "field 'tv_kolesterol_d'");
    target.tv_kolesterol_d = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492949, "field 'tv_disclaimer'");
    target.tv_disclaimer = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492934, "field 'tv_keep'");
    target.tv_keep = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492935, "field 'tv_keep_d'");
    target.tv_keep_d = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492950, "field 'tv_disclaimer_d'");
    target.tv_disclaimer_d = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492944, "field 'tv_sleep_d'");
    target.tv_sleep_d = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492938, "field 'tv_avoid_d'");
    target.tv_avoid_d = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492943, "field 'tv_sleep'");
    target.tv_sleep = (android.widget.TextView) view;
  }

  public static void reset(com.raay.jagajantung.fragments.TipsFragment target) {
    target.btn_tnc = null;
    target.tv_exer = null;
    target.tv_avoid = null;
    target.tv_exer_d = null;
    target.tv_kolesterol = null;
    target.tv_smoke_d = null;
    target.tv_eat_d = null;
    target.tv_smoke = null;
    target.tv_eat = null;
    target.tv_h = null;
    target.tv_h_d = null;
    target.tv_kolesterol_d = null;
    target.tv_disclaimer = null;
    target.tv_keep = null;
    target.tv_keep_d = null;
    target.tv_disclaimer_d = null;
    target.tv_sleep_d = null;
    target.tv_avoid_d = null;
    target.tv_sleep = null;
  }
}
