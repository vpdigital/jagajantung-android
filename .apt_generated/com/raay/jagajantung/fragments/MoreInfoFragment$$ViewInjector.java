// Generated code from Butter Knife. Do not modify!
package com.raay.jagajantung.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class MoreInfoFragment$$ViewInjector {
  public static void inject(Finder finder, final com.raay.jagajantung.fragments.MoreInfoFragment target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492879, "field 'btn_back'");
    target.btn_back = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492881, "field 'btn_tnc'");
    target.btn_tnc = (android.widget.Button) view;
  }

  public static void reset(com.raay.jagajantung.fragments.MoreInfoFragment target) {
    target.btn_back = null;
    target.btn_tnc = null;
  }
}
