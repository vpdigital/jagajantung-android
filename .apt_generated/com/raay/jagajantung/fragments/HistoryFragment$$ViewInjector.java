// Generated code from Butter Knife. Do not modify!
package com.raay.jagajantung.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class HistoryFragment$$ViewInjector {
  public static void inject(Finder finder, final com.raay.jagajantung.fragments.HistoryFragment target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492881, "field 'btn_tnc'");
    target.btn_tnc = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492885, "field 'tbl_detak'");
    target.tbl_detak = (android.widget.TableLayout) view;
    view = finder.findRequiredView(source, 2131492886, "field 'img_mail'");
    target.img_mail = (android.widget.ImageView) view;
  }

  public static void reset(com.raay.jagajantung.fragments.HistoryFragment target) {
    target.btn_tnc = null;
    target.tbl_detak = null;
    target.img_mail = null;
  }
}
