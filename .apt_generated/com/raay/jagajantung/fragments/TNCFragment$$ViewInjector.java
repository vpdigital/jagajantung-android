// Generated code from Butter Knife. Do not modify!
package com.raay.jagajantung.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class TNCFragment$$ViewInjector {
  public static void inject(Finder finder, final com.raay.jagajantung.fragments.TNCFragment target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492879, "field 'btn_back'");
    target.btn_back = (android.widget.Button) view;
  }

  public static void reset(com.raay.jagajantung.fragments.TNCFragment target) {
    target.btn_back = null;
  }
}
