// Generated code from Butter Knife. Do not modify!
package com.raay.jagajantung.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class BloodPressureActivity$$ViewInjector {
  public static void inject(Finder finder, final com.raay.jagajantung.activities.BloodPressureActivity target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492909, "field 'btn_save'");
    target.btn_save = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492881, "field 'btn_tnc'");
    target.btn_tnc = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492908, "field 'btn_skip'");
    target.btn_skip = (android.widget.Button) view;
  }

  public static void reset(com.raay.jagajantung.activities.BloodPressureActivity target) {
    target.btn_save = null;
    target.btn_tnc = null;
    target.btn_skip = null;
  }
}
