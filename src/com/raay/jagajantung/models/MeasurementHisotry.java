package com.raay.jagajantung.models;

import java.io.Serializable;

public class MeasurementHisotry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private int heartRate;
	private int systolic;
	private int diastolic;

	// < 60 = rested, 60-90 = normal, > 90 = over
	private enum BPM_STATE {
		RESTED, NORMAL, OVER
	}

	private enum CONDITION {
		Normal, Banyak_Pikiran, Santai
	}

	private String status;
	private String created_date;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(int heartRate) {
		this.heartRate = heartRate;
	}

	public int getSystolic() {
		return systolic;
	}

	public void setSystolic(int systolic) {
		this.systolic = systolic;
	}

	public int getDiastolic() {
		return diastolic;
	}

	public void setDiastolic(int diastolic) {
		this.diastolic = diastolic;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

}
