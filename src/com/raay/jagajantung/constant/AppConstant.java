package com.raay.jagajantung.constant;

public interface AppConstant {

	public static final String APP_NAME = "JAGA JANTUNG";

	public static final String NODATA = "-";
	public static final String NORMAL = "Normal";
	public static final String RESTED = "Rested";
	public static final String OVER = "Over";

	public static final String CONDITION_NORMAL = "Normal";
	public static final String CONDITION_OVER = "Banyak Pikiran";
	public static final String CONDITION_RELAX = "Santai";

	public static final String SHARED_KEY = "shared_jagajantung";

	public static final String GENDER_MALE = "MALE";
	public static final String GENDER_FEMALE = "FEMALE";

	public static final int TAB_TIPS = 0;
	public static final int TAB_MEASURE = 1;
	public static final int TAB_HISTORY = 2;
	public static final int GO_TO_TNC = 3;

	public static final int YES = 1;
	public static final int NO = 0;

	public static final String PASSED_INT = "PASSED_INT";
	public static final String PASSED_STRING = "PASSED_STRING";

	public static final String EDIT_STATES = "EDIT";
	public static final int EDIT_INT = 7;
	public static final int NOT_EDIT = 9;

	public static final int NO_DATA = 0;

	public static final int NORMAL_SYS = 120;

	public static final int NORMAL_DIA = 80;

	public static final int DB_OPERATION_FAILED = -1;

	public static final String HEART_RATE_FIELD = "Heart Rate : ";
	public static final String CONDITION_FIELD = "Condition : ";
	public static final String DIASTOLIC_FIELD = "Diastolik : ";
	public static final String SYSTOLIC_FIELD = "Sistolik : ";
	public static final String DATE_FIELD = "Tanggal : ";

}
