package com.raay.jagajantung.constant;

public interface DBConstant {

	// DATABASE VERSION
	public static final int DATABASE_VERSION = 1;
	// DATABASE NAME
	public static final String DATABASE_NAME = "jagajantung.db";
	// TABLE NAME
	public static final String TABLE_MEASUREMENT = "measurement_history";

	// COMMON FIELD
	public static final String ID = "id";

	// TABLE MEASUREMENT
	public static final String HEART_RATE = "heart_rate";
	public static final String SYSTOLIC = "systolic";
	public static final String DIASTOLIC = "diastolic";
	public static final String CONDITION = "condition";
	public static final String CREATED_DATE = "created_date";

	public static final String CREATE_TABLE_MEASUREMENT = String
			.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s INTEGER, %s INTEGER, %s INTEGER, %s TEXT, %s DATETIME DEFAULT CURRENT_TIMESTAMP)",
					TABLE_MEASUREMENT, ID, HEART_RATE, SYSTOLIC, DIASTOLIC,
					CONDITION, CREATED_DATE);
}
