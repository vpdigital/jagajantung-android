package com.raay.jagajantung.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

/**
 * Interface of BaseActivity
 * 
 * This Interface is used to show the blueprint of BaseActivity
 * 
 * @author Riki Ari Andri Yani
 * @version 1.0
 * @since 2014
 */

public interface IBaseActivity {
	public void setLayout(int layout, int title);

	public void setHeader(int title);

	public void goToScreen(Context appContext, Class<?> cls);

	public void goToScreen(Context appContext, Class<?> cls, Bundle data);

	public void goToScreen(Context appContext, Class<?> cls, int animEnter,
			int animExit);

	public void goToScreen(Context appContext, Class<?> cls, int animEnter,
			int animExit, Bundle data);

	public boolean canUseAnimation();

	public void showAlertMessage(String title, String message);

	public void showMessage(String msg);

	public FragmentManager getSupportedFragment();

	public void changeLanguage(String lang);

	public Context getContext();
}
