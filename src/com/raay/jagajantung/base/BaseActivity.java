package com.raay.jagajantung.base;

import java.io.Serializable;
import java.util.Locale;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;

import com.raay.jagajantung.R;
import com.raay.jagajantung.fragments.HeartMeasureContainer;
import com.raay.jagajantung.fragments.HistoryContainer;
import com.raay.jagajantung.fragments.TipsContainer;
import com.raay.jagajantung.widgets.VDialog;

/**
 * BaseActivity
 * 
 * This Activity is used to be a master activity of others (code-reuse)
 * 
 * @author Riki Ari Andri Yani
 * @version 1.0
 * @since 2014
 */

public class BaseActivity extends FragmentActivity implements IBaseActivity,
		OnClickListener {

	protected Context context;

	// private String channel;
	// AppUtils utils;

	// private MenuDrawer mMenuDrawer;

	// private int limit = 0;

	// private ResideMenu resideMenu;

	private DrawerLayout mDrawerLayout;
	// private ListView mDrawerList;

	private LinearLayout mDrawerList;

	public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION = "com.raay.jagajantung.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";
	private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
	public static final IntentFilter INTENT_FILTER = createIntentFilter();

	private static final String TIPS_TAB_TAG = "tips_tab";
	private static final String HEART_MEASURE_TAG = "heart_tab";
	private static final String HISTORY_TAB_TAG = "history_tab";

	private FragmentTabHost mTabHost;

	public void onCreate(Bundle savedInstanceState, int headerTab) {
		super.onCreate(savedInstanceState);

		setContentView(headerTab);
		context = this;
		ButterKnife.inject(this);

		InitView();

	}

	public void setCurrentTab(int tab) {
		mTabHost.setCurrentTab(tab);
	}

	protected void onCreate(Bundle savedInstanceState, int layout,
			boolean noheadermenu) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.header_drawer);

		if (noheadermenu) {
			setContentView(layout);
			context = this;
			ButterKnife.inject(this);

			InitView();
		} else {
			setLayout(layout);
			// setMenuDrawer();
			context = this;
			ButterKnife.inject(this);
			// setNavigation();

			// setMenuDrawer();
			// btn_logout.setVisibility(View.INVISIBLE);

		}

		// registerBaseActivityReceiver();

	}

	public void setLayout(int layout) {
		setContentView(R.layout.header_polos);

		LayoutInflater layoutInflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		FrameLayout parentLayout = (FrameLayout) findViewById(R.id.frame_container);
		parentLayout.removeAllViewsInLayout();

		View childLayout = layoutInflater.inflate(layout,
				(ViewGroup) findViewById(layout), false);
		childLayout.setLayoutParams(new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));

		parentLayout.addView(childLayout);

	}

	private void InitView() {
		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(TIPS_TAB_TAG),
						R.drawable.tab_indicator_gen,
						getString(R.string.tips_tab),
						R.drawable.ic_action_hist_64), TipsContainer.class,
				null);
		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(HEART_MEASURE_TAG),
						R.drawable.tab_indicator_gen,
						getString(R.string.measure_tab),
						R.drawable.ic_action_pulse_64),
				HeartMeasureContainer.class, null);
		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(HISTORY_TAB_TAG),
						R.drawable.tab_indicator_gen,
						getString(R.string.riwayat_tab),
						R.drawable.ic_action_tips_64), HistoryContainer.class,
				null);
		mTabHost.setCurrentTab(1);
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {

			}
		});

	}

	public void InitView(Class<?> focused) {

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		mTabHost.clearAllTabs();

		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(TIPS_TAB_TAG),
						R.drawable.tab_indicator_gen, "Tips",
						R.drawable.ic_action_hist_64), TipsContainer.class,
				null);
		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(HEART_MEASURE_TAG),
						R.drawable.tab_indicator_gen, "Mengukur",
						R.drawable.ic_action_pulse_64), focused, null);
		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(HISTORY_TAB_TAG),
						R.drawable.tab_indicator_gen, "Riwayat",
						R.drawable.ic_action_tips_64), HistoryContainer.class,
				null);
		mTabHost.setCurrentTab(1);
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {

			}
		});

	}

	public void setTab(int tab) {
		mTabHost.setCurrentTab(tab);
	}

	private TabSpec setIndicator(Context ctx, TabSpec spec, int resid,
			String string, int genresIcon) {
		View v = LayoutInflater.from(ctx).inflate(R.layout.tab_item, null);
		v.setBackgroundResource(resid);
		TextView tv = (TextView) v.findViewById(R.id.txt_tabtxt);
		ImageView img = (ImageView) v.findViewById(R.id.img_tabtxt);

		tv.setText(string);
		img.setBackgroundResource(genresIcon);
		return spec.setIndicator(v);
	}

	public void goToScreen(Context appContext, Class<?> cls, int animEnter,
			int animExit) {
		Intent i = new Intent(appContext, cls);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		startActivity(i);
		if (canUseAnimation()) {
			overridePendingTransition(animEnter, animExit);
		}
	}

	public void goToScreen(Context appContext, Class<?> cls, int animEnter,
			int animExit, Bundle extras) {
		Intent i = new Intent(appContext, cls);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		i.putExtras(extras);
		startActivity(i);
		if (canUseAnimation()) {
			overridePendingTransition(animEnter, animExit);
		}
	}

	public void goToScreen(Context appContext, Class<?> cls) {
		Intent i = new Intent(appContext, cls);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		startActivity(i);
		if (canUseAnimation()) {
			overridePendingTransition(R.anim.activity_open_translate,
					R.anim.activity_close_scale);
		}
	}

	public void goToScreen(Context appContext, Class<?> cls, Bundle data) {
		Intent i = new Intent(appContext, cls);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.putExtras(data);
		startActivity(i);
		if (canUseAnimation()) {
			overridePendingTransition(R.anim.open_next,
					R.anim.activity_close_scale);
		}
	}

	public void goToScreen(Context appContext, Class<?> cls, Serializable data) {
		Intent i = new Intent(appContext, cls);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.putExtra("data", data);
		startActivity(i);
		if (canUseAnimation()) {
			overridePendingTransition(R.anim.open_next, R.anim.close_main);
		}
	}

	public boolean canUseAnimation() {
		int currentVersion = android.os.Build.VERSION.SDK_INT;
		if (currentVersion < android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return false;
		}
		return true;
	}

	@Override
	public void showMessage(String msg) {
		StringBuilder str = new StringBuilder();
		str.append(msg);
		Toast.makeText(context, str, Toast.LENGTH_LONG).show();

	}

	public void showAlertMessage(String title, String message) {
		FragmentManager fm = getSupportFragmentManager();
		VDialog dialog = new VDialog(title, message);
		dialog.show(fm, "alert_dialog");
	}

	public void showAlertMessage(String title, String message,
			android.content.DialogInterface.OnClickListener positiveClick) {
		FragmentManager fm = getSupportFragmentManager();
		VDialog dialog = new VDialog(title, message, positiveClick);
		dialog.show(fm, "alert_dialog");
	}

	public void showAlertMessage(String title, String message,
			android.content.DialogInterface.OnClickListener positiveClick,
			android.content.DialogInterface.OnClickListener negativeClick) {
		FragmentManager fm = getSupportFragmentManager();
		VDialog dialog = new VDialog(title, message, positiveClick,
				negativeClick);
		dialog.show(fm, "alert_dialog");
	}

	public void switchToFragment(Fragment fr) {
		FrameLayout parentLayout = (FrameLayout) findViewById(R.id.realtabcontent);
		parentLayout.removeAllViewsInLayout();

		if (fr != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
					.replace(R.id.realtabcontent, fr).commit();
		}
	}

	public void switchToFragment(Fragment fr, int animEnter, int animExit) {
		FrameLayout parentLayout = (FrameLayout) findViewById(R.id.frame_container);
		parentLayout.removeAllViewsInLayout();

		if (fr != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.setCustomAnimations(animEnter, animExit)
					.replace(R.id.frame_container, fr).commit();
		}
	}

	public void changeLanguage(String lang) {
		String languageToLoad = lang;
		Locale locale = new Locale(languageToLoad);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
	}

	@Override
	public FragmentManager getSupportedFragment() {
		return super.getSupportFragmentManager();
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		default:
			break;
		}
	}

	private static IntentFilter createIntentFilter() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
		return filter;
	}

	protected void registerBaseActivityReceiver() {
		registerReceiver(baseActivityReceiver, INTENT_FILTER);
	}

	protected void unRegisterBaseActivityReceiver() {
		unregisterReceiver(baseActivityReceiver);
	}

	public class BaseActivityReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction()
					.equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)) {
				finish();
			}
		}
	}

	protected void closeAllActivities() {
		sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// unRegisterBaseActivityReceiver();
		// closeAllActivities();
	}

	@Override
	public void setLayout(int layout, int title) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setHeader(int title) {
		// TODO Auto-generated method stub

	}

}
