package com.raay.jagajantung.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.raay.jagajantung.constant.AppConstant;
import com.raay.jagajantung.constant.DBConstant;
import com.raay.jagajantung.models.MeasurementHisotry;

public class DBHelper extends SQLiteOpenHelper {

	Cursor itemsCursor;
	Context context;
	String LOG;

	public DBHelper(Context context) {
		super(context, DBConstant.DATABASE_NAME, null,
				DBConstant.DATABASE_VERSION);
		this.context = context;
		LOG = DBHelper.class.getName();

		Log.i(LOG, "Initializer");
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		if (!db.isReadOnly()) {
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DBConstant.CREATE_TABLE_MEASUREMENT);

		Log.i(LOG, "Create Database");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP IF EXISTS " + DBConstant.TABLE_MEASUREMENT);

		onCreate(db);
	}

	/**
	 * "PROFILE" TABLE METHODS
	 */

	/**
	 * "MEASUREMENT" TABLE METHODS
	 */

	// CREATE A MEASUREMENT

	public long createMeasurement(MeasurementHisotry data) {
		SQLiteDatabase db = getWritableDatabase();

		ContentValues values = new ContentValues();
		// values.put(DBConstant.ID, data.getId());
		values.put(DBConstant.HEART_RATE, data.getHeartRate());
		values.put(DBConstant.SYSTOLIC, data.getSystolic());
		values.put(DBConstant.DIASTOLIC, data.getDiastolic());
		values.put(DBConstant.CONDITION, data.getStatus());
		values.put(DBConstant.CREATED_DATE, data.getCreated_date());

		long result = AppConstant.DB_OPERATION_FAILED;

		result = db.insert(DBConstant.TABLE_MEASUREMENT, null, values);

		return result;
	}

	// GET SINGLE MEASUREMENT
	public MeasurementHisotry getSingleMeasurement() {
		SQLiteDatabase db = this.getReadableDatabase();

		String query = String.format("SELECT * FROM %s",
				DBConstant.TABLE_MEASUREMENT);
		Log.e(LOG, query);

		Cursor c = db.rawQuery(query, null);
		if (c != null) {
			c.moveToFirst();
		}

		MeasurementHisotry data = new MeasurementHisotry();
		data.setId(c.getInt(c.getColumnIndex(DBConstant.ID)));
		data.setHeartRate(c.getInt(c.getColumnIndex(DBConstant.HEART_RATE)));
		data.setSystolic(c.getInt(c.getColumnIndex(DBConstant.SYSTOLIC)));
		data.setDiastolic(c.getInt(c.getColumnIndex(DBConstant.DIASTOLIC)));
		data.setStatus(c.getString(c.getColumnIndex(DBConstant.CONDITION)));
		data.setCreated_date(c.getString(c
				.getColumnIndex(DBConstant.CREATED_DATE)));

		c.close();
		return data;
	}

	// GET ALL MEASUREMENT
	public List<MeasurementHisotry> getAllHistory() {
		List<MeasurementHisotry> historis = new ArrayList<MeasurementHisotry>();

		String selectQuery = "SELECT  * FROM " + DBConstant.TABLE_MEASUREMENT;

		Log.e(LOG, selectQuery);

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				MeasurementHisotry data = new MeasurementHisotry();
				data.setId(c.getInt(c.getColumnIndex(DBConstant.ID)));
				data.setHeartRate(c.getInt(c
						.getColumnIndex(DBConstant.HEART_RATE)));
				data.setSystolic(c.getInt(c.getColumnIndex(DBConstant.SYSTOLIC)));
				data.setDiastolic(c.getInt(c
						.getColumnIndex(DBConstant.DIASTOLIC)));
				data.setStatus(c.getString(c
						.getColumnIndex(DBConstant.CONDITION)));
				data.setCreated_date(c.getString(c
						.getColumnIndex(DBConstant.CREATED_DATE)));

				historis.add(data);
			} while (c.moveToNext());
		}

		return historis;
	}

	// UPDATE MEASUREMENT
	public int updateHistory(MeasurementHisotry data) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(DBConstant.ID, data.getId());
		values.put(DBConstant.HEART_RATE, data.getHeartRate());
		values.put(DBConstant.SYSTOLIC, data.getSystolic());
		values.put(DBConstant.DIASTOLIC, data.getDiastolic());
		values.put(DBConstant.CONDITION, data.getStatus());
		values.put(DBConstant.CREATED_DATE, data.getCreated_date());

		return db.update(DBConstant.TABLE_MEASUREMENT, values, DBConstant.ID
				+ " = ? ", new String[] { String.valueOf(data.getId()) });

	}

	// DELETE MEASUREMENT
	public int deleteHistory(long id) {
		SQLiteDatabase db = this.getWritableDatabase();

		int result = AppConstant.DB_OPERATION_FAILED;

		result = db.delete(DBConstant.TABLE_MEASUREMENT,
				DBConstant.ID + " = ?", new String[] { String.valueOf(id) });

		return result;
	}

	// GET COUNT
	public int getHistoryCount() {
		String countQuery = String.format("SELECT %s FROM %s", DBConstant.ID,
				DBConstant.TABLE_MEASUREMENT);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();
		cursor.close();

		return count;
	}

	// CLOSING DATABASE
	public void closeDB() {
		SQLiteDatabase db = this.getReadableDatabase();
		if (db != null && db.isOpen())
			db.close();

		if (itemsCursor != null) {
			itemsCursor.close();
		}
	}

	// RESETING DATABASE
	public void resetDB(String dbName) {
		context.deleteDatabase(dbName);
	}

	public String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date date = new Date();
		return dateFormat.format(date);
	}

}
