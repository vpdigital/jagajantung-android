package com.raay.jagajantung.widgets;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.raay.jagajantung.R;

public class VProgressDialog extends ProgressDialog {

	private AnimationDrawable animation;

	public static ProgressDialog ctor(Context context) {
		VProgressDialog dialog = new VProgressDialog(context);
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);
		return dialog;
	}

	public VProgressDialog(Context context) {
		super(context);

	}

	public VProgressDialog(Context context, int theme) {
		super(context, theme);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.flat_progress_layout);

		ImageView la = (ImageView) findViewById(R.id.animation);
		la.setBackgroundResource(R.drawable.flat_progress);
		animation = (AnimationDrawable) la.getBackground();
	}

	@Override
	public void show() {
		super.show();
		animation.start();
	}

	@Override
	public void dismiss() {
		super.dismiss();
		animation.stop();
	}
}
