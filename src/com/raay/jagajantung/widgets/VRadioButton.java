package com.raay.jagajantung.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

public class VRadioButton extends RadioButton {

	public VRadioButton(Context context) {
		super(context);

	}

	public VRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	public VRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

	}

	// intercept Typeface change and set it with our custom font
	public void setTypeface(Typeface tf, int style) {
		switch (style) {
		case Typeface.NORMAL:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Regular.ttf"));
			break;

		case Typeface.BOLD:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Bold.ttf"));
			break;

		case Typeface.ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Italic.ttf"));
			break;

		case Typeface.BOLD_ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-BoldItalic.ttf"));
			break;
		default:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Regular.ttf"));
			break;
		}

	}

}
