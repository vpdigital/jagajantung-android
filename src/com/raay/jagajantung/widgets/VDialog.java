package com.raay.jagajantung.widgets;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Android Custom Message Dialog Component
 * 
 * This is used same as android default Dialog component
 * 
 * @author Riki Ari Andri Yani
 * @version 1.0
 * @since 2014
 */

public class VDialog extends DialogFragment {

	private String title;
	private String message;
	private OnClickListener posClickListener;
	private OnClickListener negClickListener;

	public VDialog(String title, String message) {
		this.title = title;
		this.message = message;
	}

	public VDialog(String title, String message,
			OnClickListener posClickListener) {
		this.title = title;
		this.message = message;
		this.posClickListener = posClickListener;
	}

	public VDialog(String title, String message,
			OnClickListener posClickListener, OnClickListener negClickListener) {
		this.title = title;
		this.message = message;
		this.posClickListener = posClickListener;
		this.negClickListener = negClickListener;
	}

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(message);
		builder.setPositiveButton("OK", posClickListener);
		builder.setNegativeButton("Cancel", negClickListener);
		builder.setTitle(title);
		builder.setCancelable(false);

		Dialog dialog = builder.create();
		return dialog;
	}

}
