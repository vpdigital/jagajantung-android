package com.raay.jagajantung.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.raay.jagajantung.R;

/**
 * Android Custom EditText Component
 * 
 * This is used same as android default EditText component but different in font
 * typeface
 * 
 * @author Riki Ari Andri Yani
 * @version 1.0
 * @since 2014
 */

public class VEditText extends EditText {

	// The image we are going to use for the Clear button
	private Drawable imgCloseButton = getResources().getDrawable(
			R.drawable.close);

	public VEditText(Context context) {
		super(context);
		init();
	}

	public VEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public VEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	void init() {

		// Set bounds of the Clear button so it will look ok
		imgCloseButton.setBounds(0, 0, imgCloseButton.getIntrinsicWidth(),
				imgCloseButton.getIntrinsicHeight());

		// There may be initial text in the field, so we may need to display the
		// button
		handleClearButton();

		// if the Close image is displayed and the user remove his finger from
		// the button, clear it. Otherwise do nothing
		this.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				VEditText et = VEditText.this;

				if (et.getCompoundDrawables()[2] == null)
					return false;

				if (event.getAction() != MotionEvent.ACTION_UP)
					return false;

				if (event.getX() > et.getWidth() - et.getPaddingRight()
						- imgCloseButton.getIntrinsicWidth()) {
					et.setText("");
					VEditText.this.handleClearButton();
				}
				return false;
			}
		});

		// if text changes, take care of the button
		this.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				VEditText.this.handleClearButton();
			}

			@Override
			public void afterTextChanged(Editable arg0) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
		});
	}

	// intercept Typeface change and set it with our custom font
	public void setTypeface(Typeface tf, int style) {
		switch (style) {
		case Typeface.NORMAL:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/CaviarDreams.ttf"));
			break;

		case Typeface.BOLD:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/CaviarDreamsBold.ttf"));
			break;

		case Typeface.ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/CaviarDreams_Italic.ttf"));
			break;

		case Typeface.BOLD_ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(),
					"fonts/CaviarDreams_BoldItalic.ttf"));
			break;
		default:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/CaviarDreams.ttf"));
			break;
		}

	}

	void handleClearButton() {
		if (this.getText().toString().equals("")) {
			// add the clear button
			this.setCompoundDrawables(this.getCompoundDrawables()[0],
					this.getCompoundDrawables()[1], null,
					this.getCompoundDrawables()[3]);
		} else {
			// remove clear button
			this.setCompoundDrawables(this.getCompoundDrawables()[0],
					this.getCompoundDrawables()[1], imgCloseButton,
					this.getCompoundDrawables()[3]);
		}
	}
}
