package com.raay.jagajantung.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Android Custom Button Component
 * 
 * This is used same as android default button component but different in font
 * typeface
 * 
 * @author Riki Ari Andri Yani
 * @version 1.0
 * @since 2014
 */

public class VButton extends Button {

	public VButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// init();
	}

	public VButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		// init();
	}

	public VButton(Context context) {
		super(context);
		// init();
	}

	private void init() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"fonts/Roboto-Thin.ttf");
		setTypeface(tf);
	}

	// intercept Typeface change and set it with our custom font
	public void setTypeface(Typeface tf, int style) {
		switch (style) {
		case Typeface.NORMAL:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Regular.ttf"));
			break;

		case Typeface.BOLD:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Bold.ttf"));
			break;

		case Typeface.ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Italic.ttf"));
			break;

		case Typeface.BOLD_ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-BoldItalic.ttf"));
			break;
		default:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Regular.ttf"));
			break;
		}
	}

}
