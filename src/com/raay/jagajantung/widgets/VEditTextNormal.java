package com.raay.jagajantung.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.EditText;

import com.raay.jagajantung.R;

/**
 * Android Custom EditText Component
 * 
 * This is used same as android default EditText component but different in font
 * typeface
 * 
 * @author Riki Ari Andri Yani
 * @version 1.0
 * @since 2014
 */

public class VEditTextNormal extends EditText {

	// The image we are going to use for the Clear button
	private Drawable imgCloseButton = getResources().getDrawable(
			R.drawable.close);

	public VEditTextNormal(Context context) {
		super(context);

	}

	public VEditTextNormal(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

	}

	public VEditTextNormal(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	// intercept Typeface change and set it with our custom font
	public void setTypeface(Typeface tf, int style) {
		switch (style) {
		case Typeface.NORMAL:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/CaviarDreams.ttf"));
			break;

		case Typeface.BOLD:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/CaviarDreamsBold.ttf"));
			break;

		case Typeface.ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/CaviarDreams_Italic.ttf"));
			break;

		case Typeface.BOLD_ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(),
					"fonts/CaviarDreams_BoldItalic.ttf"));
			break;
		default:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/CaviarDreams.ttf"));
			break;
		}

	}

}
