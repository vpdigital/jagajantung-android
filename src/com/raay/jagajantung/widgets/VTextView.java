package com.raay.jagajantung.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.raay.jagajantung.R;

/**
 * Android Custom TextView Component
 * 
 * This is used same as android default TextView component but different in font
 * typeface
 * 
 * @author Riki Ari Andri Yani
 * @version 1.0
 * @since 2014
 */

public class VTextView extends TextView {

	public VTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public VTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);

	}

	public VTextView(Context context) {
		super(context);
		init(null);
	}

	private void init(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs,
					R.styleable.VTextView);
			String fontName = a.getString(R.styleable.VTextView_fontName);
			if (fontName != null) {
				Typeface myTypeface = Typeface.createFromAsset(getContext()
						.getAssets(), "fonts/" + fontName);
				setTypeface(myTypeface);
			}
			a.recycle();
		}
	}

	// intercept Typeface change and set it with our custom font
	public void setTypeface(Typeface tf, int style) {
		switch (style) {
		case Typeface.NORMAL:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Regular.ttf"));
			break;

		case Typeface.BOLD:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Bold.ttf"));
			break;

		case Typeface.ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Italic.ttf"));
			break;

		case Typeface.BOLD_ITALIC:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-BoldItalic.ttf"));
			break;
		default:
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/OpenSans-Regular.ttf"));
			break;
		}

	}

}