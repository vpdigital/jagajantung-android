package com.raay.jagajantung.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.raay.jagajantung.constant.AppConstant;

import android.util.Log;

public class Utils {

	private enum INDONESIAN_DAY {
		Senin, Selasa, Rabu, Kamis, Jumat, Sabtu, Minggu
	}

	public static String getIndonesiaDay(int day) {

		String id_day;

		switch (day) {
		case Calendar.SUNDAY:
			id_day = INDONESIAN_DAY.Minggu.toString();
			break;
		case Calendar.MONDAY:
			id_day = INDONESIAN_DAY.Senin.toString();
			break;
		case Calendar.TUESDAY:
			id_day = INDONESIAN_DAY.Selasa.toString();
			break;
		case Calendar.WEDNESDAY:
			id_day = INDONESIAN_DAY.Rabu.toString();
			break;
		case Calendar.THURSDAY:
			id_day = INDONESIAN_DAY.Kamis.toString();
			break;
		case Calendar.FRIDAY:
			id_day = INDONESIAN_DAY.Jumat.toString();
			break;
		case Calendar.SATURDAY:
			id_day = INDONESIAN_DAY.Sabtu.toString();
			break;

		default:
			id_day = "INVALID DAY";
			break;
		}

		return id_day;
	}

	public static String getDateForResult(String dateStr) {
		Calendar t = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.getDefault());
		java.util.Date dt = null;
		try {
			dt = sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Log.e("ERROR", e.getMessage());
		}
		t.setTime(dt);

		String strDay = getIndonesiaDay(t.get(Calendar.DAY_OF_WEEK));

		SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yyyy",
				Locale.getDefault());

		return strDay + ", " + newFormat.format(dt);

	}

	public static Calendar getRealDate(String dateStr) {
		Calendar t = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.getDefault());
		java.util.Date dt = null;
		try {
			dt = sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Log.e("ERROR", e.getMessage());
		}
		t.setTime(dt);

		return t;
	}

	public static String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static String getDateOnly(String date) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date newDate = dateFormat.parse(date);

		SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yy",
				Locale.getDefault());

		return newFormat.format(newDate);

	}

	public static String getTimeOnly(String date) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date newDate = dateFormat.parse(date);

		SimpleDateFormat newFormat = new SimpleDateFormat("HH:mm",
				Locale.getDefault());

		return newFormat.format(newDate);
	}

	public static String getGrahpDate(String date, String format)
			throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date newDate = dateFormat.parse(date);

		SimpleDateFormat newFormat = new SimpleDateFormat(format,
				Locale.getDefault());

		return newFormat.format(newDate);
	}

	public static String getHeartStatus(int rate) {
		if (rate < 60) {
			return AppConstant.RESTED;
		} else if (rate >= 60 && rate <= 90) {
			return AppConstant.NORMAL;
		} else {
			return AppConstant.OVER;
		}
	}

	public static String formatDecimalHeartRate(String num) {
		return String.format("%03d", num);
	}
}
