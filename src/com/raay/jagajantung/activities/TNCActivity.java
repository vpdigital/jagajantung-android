package com.raay.jagajantung.activities;

import android.os.Bundle;

import com.raay.jagajantung.R;
import com.raay.jagajantung.base.BaseActivity;
import com.raay.jagajantung.fragments.TNCContainer;

public class TNCActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.header_tab);

		InitView(TNCContainer.class);
	}

}
