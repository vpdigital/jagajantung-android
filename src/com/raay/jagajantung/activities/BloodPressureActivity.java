package com.raay.jagajantung.activities;

import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import butterknife.InjectView;

import com.raay.jagajantung.DashboardActivity;
import com.raay.jagajantung.R;
import com.raay.jagajantung.base.BaseActivity;
import com.raay.jagajantung.constant.AppConstant;
import com.raay.jagajantung.constant.DBConstant;
import com.raay.jagajantung.db.DBHelper;
import com.raay.jagajantung.models.MeasurementHisotry;
import com.raay.jagajantung.utils.Utils;

public class BloodPressureActivity extends BaseActivity {

	@InjectView(R.id.btn_skip)
	Button btn_skip;

	@InjectView(R.id.btn_save)
	Button btn_save;

	private int systolic = AppConstant.NORMAL_SYS;
	private int diastolic = AppConstant.NORMAL_DIA;

	private SharedPreferences shared;

	@InjectView(R.id.btn_logout)
	Button btn_tnc;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.cities_holo_layout, false);

		shared = getSharedPreferences(AppConstant.SHARED_KEY,
				Context.MODE_PRIVATE);

		final WheelView sysWheel = (WheelView) findViewById(R.id.city);
		sysWheel.setVisibleItems(4); // Number of item
		sysWheel.setWheelBackground(android.R.color.transparent);
		sysWheel.setWheelForeground(R.drawable.wheel_val);
		sysWheel.setShadowColor(0xFFcccccc, 0x99e7e7e7, 0x1Ae7e7e7);
		sysWheel.setViewAdapter(new BloodPresureAdapter(this, 0, 140));
		sysWheel.setCurrentItem(AppConstant.NORMAL_SYS);

		final WheelView diaWheel = (WheelView) findViewById(R.id.home);
		diaWheel.setVisibleItems(4); // Number of items
		diaWheel.setWheelBackground(android.R.color.transparent);
		diaWheel.setWheelForeground(R.drawable.wheel_val);
		diaWheel.setShadowColor(0xFFcccccc, 0x99e7e7e7, 0x1Ae7e7e7);
		diaWheel.setViewAdapter(new BloodPresureAdapter(this, 0, 100));
		diaWheel.setCurrentItem(AppConstant.NORMAL_DIA);

		sysWheel.addScrollingListener(new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				systolic = AppConstant.NORMAL_SYS;
			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				systolic = wheel.getCurrentItem();
			}
		});

		diaWheel.addScrollingListener(new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				diastolic = AppConstant.NORMAL_DIA;
			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				diastolic = wheel.getCurrentItem();

			}
		});

		btn_skip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor edit = shared.edit();
				edit.putInt(DBConstant.SYSTOLIC, AppConstant.NO_DATA);
				edit.putInt(DBConstant.DIASTOLIC, AppConstant.NO_DATA);
				edit.commit();

				DBHelper helper = new DBHelper(context);

				MeasurementHisotry m = getRecentData();

				long result = helper.createMeasurement(m);
				if (result != AppConstant.DB_OPERATION_FAILED) {
					showMessage("Berhasil simpan!");
					Bundle data = new Bundle();
					data.putInt(AppConstant.PASSED_INT, AppConstant.TAB_HISTORY);
					goToScreen(context, DashboardActivity.class, data);
				} else {
					showMessage("Gagal simpan, coba lagi!");
				}

			}
		});

		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor edit = shared.edit();
				edit.putInt(DBConstant.SYSTOLIC, systolic);
				edit.putInt(DBConstant.DIASTOLIC, diastolic);
				edit.commit();

				DBHelper helper = new DBHelper(context);

				MeasurementHisotry m = getRecentData();

				long result = helper.createMeasurement(m);
				if (result != AppConstant.DB_OPERATION_FAILED) {
					showMessage("Berhasil simpan!");
					Bundle data = new Bundle();
					data.putInt(AppConstant.PASSED_INT, AppConstant.TAB_HISTORY);
					goToScreen(context, DashboardActivity.class, data);
				} else {
					showMessage("Gagal simpan, coba lagi!");
				}
			}
		});

		btn_tnc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor edit = shared.edit();
				edit.putInt(DBConstant.SYSTOLIC, AppConstant.NO_DATA);
				edit.putInt(DBConstant.DIASTOLIC, AppConstant.NO_DATA);
				edit.commit();
				// TODO Auto-generated method stub

				goToScreen(context, TNCActivity.class, R.anim.fade_in,
						R.anim.fade_out);
			}
		});

	}

	private MeasurementHisotry getRecentData() {

		String createdDate = shared.getString(DBConstant.CREATED_DATE,
				Utils.getDateTime());
		String condition = shared.getString(DBConstant.CONDITION,
				AppConstant.NODATA);
		int heartR = shared.getInt(DBConstant.HEART_RATE, -1);
		int sys = systolic;
		int dia = diastolic;

		MeasurementHisotry m = new MeasurementHisotry();
		m.setCreated_date(createdDate);
		m.setStatus(condition);
		m.setHeartRate(heartR);
		m.setDiastolic(dia);
		m.setSystolic(sys);

		return m;
	}

	/**
	 * Adapter for countries
	 */
	private class BloodPresureAdapter extends AbstractWheelTextAdapter {
		/** The default min value */
		public static final int DEFAULT_MAX_VALUE = 9;

		/** The default max value */
		private static final int DEFAULT_MIN_VALUE = 0;

		// Values
		private int minValue;
		private int maxValue;

		// format
		private String format;

		/**
		 * Constructor
		 */
		protected BloodPresureAdapter(Context context) {
			super(context, R.layout.city_holo_layout, NO_RESOURCE);

			setItemTextResource(R.id.city_name);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			View view = super.getItem(index, cachedView, parent);
			return view;
		}

		/**
		 * Constructor
		 * 
		 * @param context
		 *            the current context
		 * @param minValue
		 *            the wheel min value
		 * @param maxValue
		 *            the wheel max value
		 */
		public BloodPresureAdapter(Context context, int minValue, int maxValue) {
			this(context, minValue, maxValue, null);

		}

		/**
		 * Constructor
		 * 
		 * @param context
		 *            the current context
		 * @param minValue
		 *            the wheel min value
		 * @param maxValue
		 *            the wheel max value
		 * @param format
		 *            the format string
		 */
		public BloodPresureAdapter(Context context, int minValue, int maxValue,
				String format) {
			this(context);

			this.minValue = minValue;
			this.maxValue = maxValue;
			this.format = format;
		}

		@Override
		public CharSequence getItemText(int index) {
			if (index >= 0 && index < getItemsCount()) {
				int value = minValue + index;
				return format != null ? String.format(format, value) : Integer
						.toString(value);
			}
			return null;
		}

		@Override
		public int getItemsCount() {
			return maxValue - minValue + 1;
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// unRegisterBaseActivityReceiver();
	}

	@Override
	public void onBackPressed() {
		goToScreen(context, DashboardActivity.class);
	}
}
