package com.raay.jagajantung.adapters;

import java.text.ParseException;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.raay.jagajantung.R;
import com.raay.jagajantung.constant.AppConstant;
import com.raay.jagajantung.models.MeasurementHisotry;
import com.raay.jagajantung.utils.Utils;

public class ListViewAdapter extends BaseAdapter {

	private Context context;
	private List<MeasurementHisotry> data;
	private static LayoutInflater inflater = null;

	public ListViewAdapter(Context context, List<MeasurementHisotry> data) {
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return data.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder holder;

		if (convertView == null) {
			vi = inflater.inflate(R.layout.list_history_item, null);

			holder = new ViewHolder();

			holder.tv_heart_rate = (TextView) vi
					.findViewById(R.id.tv_heart_rate);
			holder.tv_sys = (TextView) vi.findViewById(R.id.tv_bp_sys);
			holder.tv_dia = (TextView) vi.findViewById(R.id.tv_bp_dia);
			holder.tv_status = (TextView) vi.findViewById(R.id.tv_status);
			holder.tv_date = (TextView) vi.findViewById(R.id.tv_date);
			holder.tv_time = (TextView) vi.findViewById(R.id.tv_time);

			vi.setTag(holder);
		} else {
			holder = (ViewHolder) vi.getTag();
		}

		MeasurementHisotry history = data.get(position);
		// Setting all values in listview
		holder.tv_heart_rate.setText(String.valueOf(history.getHeartRate()));
		if (history.getSystolic() == 0) {
			holder.tv_sys.setText(AppConstant.NODATA);
		} else {
			holder.tv_sys.setText(String.valueOf(history.getSystolic()));
		}

		if (history.getDiastolic() == 0) {
			holder.tv_dia.setText(AppConstant.NODATA);
		} else {
			holder.tv_dia.setText(String.valueOf(history.getDiastolic()));
		}
		String status = history.getStatus();

		if (status.equalsIgnoreCase(AppConstant.NORMAL)) {
			holder.tv_status.setBackgroundResource(R.drawable.rounded_normal);
			holder.tv_status.setText(status);
		} else if (status.equalsIgnoreCase(AppConstant.RESTED)) {
			holder.tv_status.setBackgroundResource(R.drawable.rounded_rested);
			holder.tv_status.setText(status);
		} else if (status.equalsIgnoreCase(AppConstant.OVER)) {
			holder.tv_status.setBackgroundResource(R.drawable.rounded_over);
			holder.tv_status.setText(status);
		} else {
			holder.tv_status.setText(AppConstant.NODATA);
		}

		String datetime = history.getCreated_date();

		try {
			holder.tv_date.setText(Utils.getDateOnly(datetime));
			holder.tv_time.setText(Utils.getTimeOnly(datetime));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			Log.d("ERROR", e.getMessage());
		}

		return vi;
	}

	static class ViewHolder {
		TextView tv_heart_rate;
		TextView tv_sys;
		TextView tv_dia;
		TextView tv_status;
		TextView tv_date;
		TextView tv_time;

	}
}