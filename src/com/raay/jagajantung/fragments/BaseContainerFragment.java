package com.raay.jagajantung.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.raay.jagajantung.R;

public class BaseContainerFragment extends Fragment {

	public void replaceFragment(Fragment fragment, boolean addToBackStack,
			boolean animation) {
		FragmentTransaction transaction = getChildFragmentManager()
				.beginTransaction();
		if (addToBackStack) {
			transaction.addToBackStack(null);
		}

		if (animation) {
			transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
		}

		transaction.replace(R.id.container_framelayout, fragment);
		transaction.commit();
		getChildFragmentManager().executePendingTransactions();
	}

	public boolean popFragment() {
		// Log.e("Ritesh", "pop fragment: "
		// + getChildFragmentManager().getBackStackEntryCount());
		boolean isPop = false;
		if (getChildFragmentManager().getBackStackEntryCount() > 0) {
			isPop = true;
			getChildFragmentManager().popBackStack();
		}
		return isPop;
	}

}