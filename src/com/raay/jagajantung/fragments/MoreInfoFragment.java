package com.raay.jagajantung.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.raay.jagajantung.R;

public class MoreInfoFragment extends BaseFragment {

	@InjectView(R.id.home_button)
	Button btn_back;

	@InjectView(R.id.btn_logout)
	Button btn_tnc;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		container.removeAllViewsInLayout();
		View view = inflater.inflate(R.layout.fragment_more_info, container,
				false);
		ButterKnife.inject(this, view);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);

		btn_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((BaseContainerFragment) getParentFragment()).popFragment();
			}
		});

		btn_tnc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				((BaseContainerFragment) getParentFragment()).replaceFragment(
						new TNCFragment(), true, true);
			}
		});
	}
}