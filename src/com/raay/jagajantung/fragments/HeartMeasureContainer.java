package com.raay.jagajantung.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raay.jagajantung.R;
import com.ultimate.camera.fragments.CameraFragment;
import com.ultimate.camera.fragments.NativeCameraFragment;

public class HeartMeasureContainer extends BaseContainerFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.container_framelayout, container,
				false);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);
		initView();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// initView();

	}

	private void initView() {
		replaceFragment(new NativeCameraFragment(), false, true);

	}

}