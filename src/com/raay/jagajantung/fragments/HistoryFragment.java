package com.raay.jagajantung.fragments;

import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.raay.jagajantung.R;
import com.raay.jagajantung.constant.AppConstant;
import com.raay.jagajantung.db.DBHelper;
import com.raay.jagajantung.models.MeasurementHisotry;
import com.raay.jagajantung.utils.Utils;
import com.raay.jagajantung.widgets.VTextView;

public class HistoryFragment extends BaseFragment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private View mChart;

	private List<MeasurementHisotry> listData;

	@InjectView(R.id.tabel_detak)
	TableLayout tbl_detak;

	@InjectView(R.id.img_mail)
	ImageView img_mail;

	@InjectView(R.id.btn_logout)
	Button btn_tnc;
	
	public HistoryFragment(){
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		container.removeAllViewsInLayout();
		View view = inflater.inflate(R.layout.activity_main, container, false);
		ButterKnife.inject(this, view);

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		DBHelper helper = new DBHelper(getActivity());
		listData = helper.getAllHistory();

		openChart(listData);
		generateTable(listData);

		img_mail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				sendLastTen(listData);

			}
		});

		btn_tnc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				((BaseContainerFragment) getParentFragment()).replaceFragment(
						new TNCFragment(), true, true);
			}
		});

	}

	private void openChart(List<MeasurementHisotry> listData2) {

		// Creating an XYSeries for Income
		// XYSeries incomeSeries = new XYSeries("Income");
		// Creating an XYSeries for Expense
		XYSeries expenseSeries = new XYSeries("Detak Jantung");
		// Adding data to Income and Expense Series
		for (int i = 0; i < listData2.size(); i++) {
			// incomeSeries.add(i, income[i]);
			expenseSeries.add(i, listData2.get(i).getHeartRate());
		}

		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding Income Series to the dataset
		// dataset.addSeries(incomeSeries);
		// Adding Expense Series to dataset
		dataset.addSeries(expenseSeries);

		// Creating XYSeriesRenderer to customize incomeSeries
		// XYSeriesRenderer incomeRenderer = new XYSeriesRenderer();
		// incomeRenderer.setColor(Color.CYAN); // color of the graph set to
		// cyan
		// incomeRenderer.setFillPoints(true);
		// incomeRenderer.setLineWidth(2);
		// incomeRenderer.setDisplayChartValues(true);
		// incomeRenderer.setDisplayChartValuesDistance(10); // setting chart
		// value
		// distance

		// Creating XYSeriesRenderer to customize expenseSeries
		XYSeriesRenderer expenseRenderer = new XYSeriesRenderer();
		expenseRenderer.setColor(Color.RED);
		expenseRenderer.setFillPoints(true);
		expenseRenderer.setLineWidth(2);
		expenseRenderer.setDisplayChartValues(true);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer
				.setOrientation(XYMultipleSeriesRenderer.Orientation.HORIZONTAL);
		multiRenderer.setXLabels(0);
		// multiRenderer.setChartTitle("Income vs Expense Chart");
		// multiRenderer.setXTitle("Year 2014");
		// multiRenderer.setYTitle("Amount in Dollars");

		/***
		 * Customizing graphs
		 */
		// setting text size of the title
		// multiRenderer.setChartTitleTextSize(28);
		// setting text size of the axis title
		// multiRenderer.setAxisTitleTextSize(24);
		multiRenderer.setXLabelsColor(Color.RED);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		// setting text size of the graph lable
		multiRenderer.setLabelsTextSize(24);
		// setting zoom buttons visiblity
		multiRenderer.setZoomButtonsVisible(false);
		// setting pan enablity which uses graph to move on both axis
		multiRenderer.setPanEnabled(true, false);
		// setting click false on graph
		multiRenderer.setClickEnabled(false);
		// setting zoom to false on both axis
		multiRenderer.setZoomEnabled(false, false);
		// setting lines to display on y axis
		multiRenderer.setShowGridY(true);
		// setting lines to display on x axis
		multiRenderer.setShowGridX(true);
		// setting legend to fit the screen size
		multiRenderer.setFitLegend(true);
		// setting displaying line on grid
		multiRenderer.setShowGrid(false);
		// setting zoom to false
		multiRenderer.setZoomEnabled(false);
		// setting external zoom functions to false
		multiRenderer.setExternalZoomEnabled(false);
		// setting displaying lines on graph to be formatted(like using
		// graphics)
		multiRenderer.setAntialiasing(true);
		// setting to in scroll to false
		multiRenderer.setInScroll(true);
		// setting to set legend height of the graph
		// multiRenderer.setLegendHeight(30);
		// setting x axis label align
		multiRenderer.setXLabelsAlign(Align.CENTER);
		// setting y axis label to align
		multiRenderer.setYLabelsAlign(Align.LEFT);

		// setting text style
		multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL);
		// setting no of values to display in y axis
		multiRenderer.setYLabels(4);
		// setting y axis max value, Since i'm using static values inside the
		// graph so i'm setting y max value to 4000.
		// if you use dynamic values then get the max y value and set here
		// multiRenderer.setYAxisMax(140);
		multiRenderer.setYAxisMin(0);
		// setting used to move the graph on xaxiz to .5 to the right
		multiRenderer.setXAxisMin(-0.8);
		// setting max values to be display in x axis
		multiRenderer.setXAxisMax(3);
		// setting bar size or space between two bars
		multiRenderer.setBarSpacing(1);
		// Setting background color of the graph to transparent
		multiRenderer.setBackgroundColor(Color.TRANSPARENT);
		// Setting margin color of the graph to transparent
		multiRenderer.setMarginsColor(getResources().getColor(
				R.color.transparent_background));
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBarWidth(70);

		multiRenderer
				.setPanLimits(new double[] { -0.8, listData2.size(), 0, 0 });

		// setting the margin size for the graph in the order top, left, bottom,
		// right
		multiRenderer.setMargins(new int[] { 30, 0, 30, 30 });

		for (int i = 0; i < listData2.size(); i++) {
			try {
				multiRenderer.addXTextLabel(i, Utils.getGrahpDate(listData2
						.get(i).getCreated_date(), "dd - HH"));
			} catch (ParseException e) {

				e.printStackTrace();
			}
		}

		// Adding incomeRenderer and expenseRenderer to multipleRenderer
		// Note: The order of adding dataseries to dataset and renderers to
		// multipleRenderer
		// should be same
		// multiRenderer.addSeriesRenderer(incomeRenderer);
		multiRenderer.addSeriesRenderer(expenseRenderer);

		// this part is used to display graph on the xml
		LinearLayout chartContainer = (LinearLayout) getActivity()
				.findViewById(R.id.chart);
		// remove any views before u paint the chart
		chartContainer.removeAllViews();
		// drawing bar chart
		mChart = ChartFactory.getBarChartView(getActivity(), dataset,
				multiRenderer, Type.STACKED);
		// adding the view to the linearlayout

		chartContainer.addView(mChart);

	}

	private void generateTable(List<MeasurementHisotry> data) {

		for (int i = 0; i < data.size(); i++) {

			MeasurementHisotry m = data.get(i);
			TableRow tr = new TableRow(getActivity());
			tr.setGravity(Gravity.CENTER);
			TableRow.LayoutParams params = new TableRow.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

			tr.setLayoutParams(params);
			int bg = R.drawable.table_odd_bg;
			if (i % 2 != 0) {
				bg = R.drawable.table_even_bg;
			} else {
				bg = R.drawable.table_odd_bg;
			}

			try {
				tr.addView(getFormattedTextView(
						Utils.getGrahpDate(m.getCreated_date(), "dd/HH"), bg));
			} catch (ParseException e) {

				e.printStackTrace();
			}

			tr.addView(getFormattedTextView(String.valueOf(m.getHeartRate()),
					bg));
			tr.addView(getFormattedTextView(m.getStatus(), bg));
			tr.addView(getFormattedTextView(String.valueOf(m.getSystolic()), bg));
			tr.addView(getFormattedTextView(String.valueOf(m.getDiastolic()),
					bg));

			tbl_detak.addView(tr);

		}

	}

	private TextView getFormattedTextView(String text, int bg) {
		VTextView td = new VTextView(getActivity());
		android.view.ViewGroup.LayoutParams td_par = new LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		td.setLayoutParams(td_par);
		td.setGravity(Gravity.CENTER);
		td.setBackgroundResource(bg);
		td.setPadding(0, 5, 0, 5);
		td.setTextColor(getResources().getColor(R.color.secondary_text_color));
		td.setTextSize(10);
		td.setText(text);

		return td;
	}

	public Intent sendEmail(String title, String content) {
		Intent i = new Intent(Intent.ACTION_SENDTO);
		i.setData(Uri.parse("mailto:"));
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
		i.putExtra(Intent.EXTRA_SUBJECT, title);
		i.putExtra(Intent.EXTRA_TEXT, content);
		return i;
	}

	public void sendLastTen(List<MeasurementHisotry> list) {

		Comparator<MeasurementHisotry> comparator = new Comparator<MeasurementHisotry>() {

			@Override
			public int compare(MeasurementHisotry lhs, MeasurementHisotry rhs) {

				Integer i1 = lhs.getId();
				Integer i2 = rhs.getId();

				return i2.compareTo(i1);
			}
		};

		if (list != null) {

			Collections.sort(list, comparator);
			StringBuilder builder = new StringBuilder();
			int i = 0;
			for (MeasurementHisotry m : list) {
				i++;
				builder.append(String.valueOf(i + ". "));
				builder.append(AppConstant.HEART_RATE_FIELD);
				builder.append(m.getHeartRate());
				builder.append("\n");
				builder.append(AppConstant.CONDITION_FIELD);
				builder.append(m.getStatus());
				builder.append("\n");
				builder.append(AppConstant.DIASTOLIC_FIELD);
				builder.append(m.getDiastolic());
				builder.append("\n");
				builder.append(AppConstant.SYSTOLIC_FIELD);
				builder.append(m.getSystolic());
				builder.append("\n");
				builder.append(AppConstant.DATE_FIELD);
				try {
					builder.append(Utils.getGrahpDate(m.getCreated_date(),
							"dd/MMM/yyyy HH:mm"));
				} catch (ParseException e) {
					Toast.makeText(getActivity(), e.getLocalizedMessage(),
							Toast.LENGTH_SHORT).show();
				}
				builder.append("\n");
				// builder.append("------------\n");

				if (i == 10) {
					break;
				}
			}
			String message = builder.toString();
			Intent intent = sendEmail(getString(R.string.riwayat), message);
			getActivity().startActivity(intent);

		} else {
			Toast.makeText(getActivity(), "DATA KOSONG", Toast.LENGTH_SHORT)
					.show();
		}
	}

}
