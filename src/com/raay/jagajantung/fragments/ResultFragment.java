package com.raay.jagajantung.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.raay.jagajantung.R;
import com.raay.jagajantung.activities.BloodPressureActivity;
import com.raay.jagajantung.constant.AppConstant;
import com.raay.jagajantung.constant.DBConstant;
import com.ultimate.camera.fragments.NativeCameraFragment;

public class ResultFragment extends BaseFragment implements OnClickListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int heart_result;

	@InjectView(R.id.btn_result_back)
	Button btn_back;

	@InjectView(R.id.btn_result_next)
	Button btn_next;

	@InjectView(R.id.btn_result_save)
	Button btn_save;

	@InjectView(R.id.tv_heart_result)
	TextView tv_heart;

	@InjectView(R.id.radioCondition)
	RadioGroup rgCondition;

	@InjectView(R.id.tv_description)
	TextView tv_desc;

	@InjectView(R.id.btn_logout)
	Button btn_tnc;

	private SharedPreferences sharedPreferences;

	public ResultFragment() {
		super();
	}

	public ResultFragment(int hr) {
		heart_result = hr;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		container.removeAllViewsInLayout();
		View view = inflater
				.inflate(R.layout.fragment_result, container, false);

		ButterKnife.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		sharedPreferences = getActivity().getSharedPreferences(
				AppConstant.SHARED_KEY, Context.MODE_PRIVATE);

		btn_next.setOnClickListener(this);
		btn_save.setOnClickListener(this);
		btn_back.setOnClickListener(this);
		btn_tnc.setOnClickListener(this);

		tv_heart.setText(getString(R.string.heart_rate_result,
				String.valueOf(heart_result)));

		rgCondition.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				tv_desc.setVisibility(View.VISIBLE);
				btn_next.setVisibility(View.VISIBLE);
				btn_save.setVisibility(View.VISIBLE);

				String condition = AppConstant.NODATA;
				switch (checkedId) {
				case R.id.radioNormal:
					condition = AppConstant.CONDITION_NORMAL;
					break;
				case R.id.radioOverMind:
					condition = AppConstant.CONDITION_OVER;
					break;

				case R.id.radioRilex:
					condition = AppConstant.CONDITION_RELAX;
					break;

				default:
					break;
				}

				Editor edit = sharedPreferences.edit();
				edit.putInt(DBConstant.HEART_RATE, heart_result);
				edit.putString(DBConstant.CONDITION, condition);
				edit.commit();
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_result_next:
			((BaseContainerFragment) getParentFragment()).replaceFragment(
					new MoreInfoFragment(), true, true);
			break;
		case R.id.btn_result_save:
			Intent intent = new Intent(getActivity(),
					BloodPressureActivity.class);
			getActivity().startActivity(intent);
			break;
		case R.id.btn_result_back:
			((BaseContainerFragment) getParentFragment()).replaceFragment(
					new NativeCameraFragment(), false, true);
			break;
		case R.id.btn_logout:

			((BaseContainerFragment) getParentFragment()).replaceFragment(
					new TNCFragment(), true, true);
			break;
		default:
			break;
		}

	}

}
