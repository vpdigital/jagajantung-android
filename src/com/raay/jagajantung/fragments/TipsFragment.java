package com.raay.jagajantung.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.raay.jagajantung.R;

public class TipsFragment extends BaseFragment implements OnClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@InjectView(R.id.tv_healthy)
	TextView tv_h;

	@InjectView(R.id.tv_healthy_detail)
	TextView tv_h_d;

	@InjectView(R.id.tv_avoid)
	TextView tv_avoid;

	@InjectView(R.id.tv_avoid_detail)
	TextView tv_avoid_d;

	@InjectView(R.id.tv_eat)
	TextView tv_eat;

	@InjectView(R.id.tv_eat_detail)
	TextView tv_eat_d;

	@InjectView(R.id.tv_kolesterol)
	TextView tv_kolesterol;

	@InjectView(R.id.tv_kolesterol_detail)
	TextView tv_kolesterol_d;

	@InjectView(R.id.tv_keep)
	TextView tv_keep;

	@InjectView(R.id.tv_keep_detail)
	TextView tv_keep_d;

	@InjectView(R.id.tv_exer)
	TextView tv_exer;

	@InjectView(R.id.tv_exer_detail)
	TextView tv_exer_d;

	@InjectView(R.id.tv_sleep)
	TextView tv_sleep;

	@InjectView(R.id.tv_sleep_detail)
	TextView tv_sleep_d;

	@InjectView(R.id.tv_no_smoke)
	TextView tv_smoke;

	@InjectView(R.id.tv_no_smoke_detail)
	TextView tv_smoke_d;

	@InjectView(R.id.tv_disclaimer)
	TextView tv_disclaimer;

	@InjectView(R.id.tv_disclaimer_detail)
	TextView tv_disclaimer_d;

	@InjectView(R.id.btn_logout)
	Button btn_tnc;

	public TipsFragment(){
		super();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		container.removeAllViewsInLayout();
		View view = inflater.inflate(R.layout.fragment_tips, container, false);
		ButterKnife.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		getActivity().findViewById(R.id.img_avoid).setOnClickListener(this);
		getActivity().findViewById(R.id.img_disclaimer)
				.setOnClickListener(this);
		getActivity().findViewById(R.id.img_eat).setOnClickListener(this);
		getActivity().findViewById(R.id.img_sleep).setOnClickListener(this);
		getActivity().findViewById(R.id.img_exercise).setOnClickListener(this);
		getActivity().findViewById(R.id.img_healthy).setOnClickListener(this);
		getActivity().findViewById(R.id.img_keep).setOnClickListener(this);
		getActivity().findViewById(R.id.img_kolesterol)
				.setOnClickListener(this);
		getActivity().findViewById(R.id.img_no_smoke).setOnClickListener(this);
		btn_tnc.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_avoid:
			send(tv_avoid.getText().toString(), tv_avoid_d.getText().toString());
			break;
		case R.id.img_disclaimer:
			send(tv_disclaimer.getText().toString(), tv_disclaimer_d.getText()
					.toString());
			break;
		case R.id.img_eat:
			send(tv_eat.getText().toString(), tv_eat_d.getText().toString());
			break;
		case R.id.img_exercise:
			send(tv_exer.getText().toString(), tv_exer_d.getText().toString());
			break;
		case R.id.img_healthy:
			send(tv_h.getText().toString(), tv_h_d.getText().toString());
			break;
		case R.id.img_keep:
			send(tv_keep.getText().toString(), tv_keep_d.getText().toString());
			break;
		case R.id.img_kolesterol:
			send(tv_kolesterol.getText().toString(), tv_kolesterol_d.getText()
					.toString());
			break;
		case R.id.img_no_smoke:
			send(tv_smoke.getText().toString(), tv_smoke_d.getText().toString());
			break;
		case R.id.img_sleep:
			send(tv_sleep.getText().toString(), tv_sleep_d.getText().toString());
			break;

		case R.id.btn_logout:

			((BaseContainerFragment) getParentFragment()).replaceFragment(
					new TNCFragment(), true, true);
			break;

		default:
			break;
		}
	}

	private void send(String title, String message) {
		List<Intent> targetedShareIntents = new ArrayList<Intent>();

		targetedShareIntents.add(sendEmail(title, message));
		targetedShareIntents.add(sendSMS(title, message));

		Intent chooserIntent = Intent.createChooser(
				targetedShareIntents.remove(0), getString(R.string.send_with));

		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
				targetedShareIntents.toArray(new Parcelable[] {}));

		startActivity(chooserIntent);
	}

	public Intent sendEmail(String title, String content) {
		Intent i = new Intent(Intent.ACTION_SENDTO);
		i.setData(Uri.parse("mailto:"));
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
		i.putExtra(Intent.EXTRA_SUBJECT, title);
		i.putExtra(Intent.EXTRA_TEXT, content);
		return i;
	}

	public Intent sendSMS(String title, String content) {
		Intent sms = new Intent(Intent.ACTION_VIEW);
		sms.putExtra("sms_body", title + "\n\n" + content);
		sms.setType("vnd.android-dir/mms-sms");

		return sms;

	}
}