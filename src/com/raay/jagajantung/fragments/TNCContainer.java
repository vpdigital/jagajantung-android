package com.raay.jagajantung.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;

import com.raay.jagajantung.R;

public class TNCContainer extends BaseContainerFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		container.removeAllViewsInLayout();
		View view = inflater.inflate(R.layout.container_framelayout, null);
		ButterKnife.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		initView();

	}

	private void initView() {
		replaceFragment(new TNCFragment(), false, true);
	}

}