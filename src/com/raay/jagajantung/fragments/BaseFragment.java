package com.raay.jagajantung.fragments;

import java.io.Serializable;

import android.support.v4.app.Fragment;
import android.widget.Toast;

public class BaseFragment extends Fragment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void showMessage(String message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}
}
