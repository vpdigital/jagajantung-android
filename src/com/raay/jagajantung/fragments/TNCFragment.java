package com.raay.jagajantung.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.raay.jagajantung.R;
import com.raay.jagajantung.activities.BloodPressureActivity;

public class TNCFragment extends BaseFragment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@InjectView(R.id.home_button)
	Button btn_back;

	public TNCFragment() {
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		container.removeAllViewsInLayout();
		View view = inflater.inflate(R.layout.fragment_tnc, container, false);
		ButterKnife.inject(this, view);

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		btn_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean result = ((BaseContainerFragment) getParentFragment())
						.popFragment();
				if (result == false) {
					Intent intent = new Intent(getActivity(),
							BloodPressureActivity.class);
					// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					getActivity().startActivity(intent);
				}
			}
		});
	}

}
