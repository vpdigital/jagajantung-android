package com.raay.jagajantung;

import android.os.Bundle;
import android.widget.Button;

import com.raay.jagajantung.base.BaseActivity;
import com.raay.jagajantung.constant.AppConstant;

public class DashboardActivity extends BaseActivity {

	Button btn_mulai;

	int progressInt = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.header_tab);

		Bundle data = getIntent().getExtras();
		if (data != null) {
			int tab = data.getInt(AppConstant.PASSED_INT);

			setCurrentTab(tab);

		}
	}

}
