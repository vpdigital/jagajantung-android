package com.raay.jagajantung;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.raay.jagajantung.db.DBHelper;

/**
 * Splash Activity of The Application
 * 
 * This activity is used as Splash Screen.
 * 
 * @author Riki Ari Andri Yani
 * @version 1.0
 * @since 2014
 */

public class SplashActivity extends Activity {

	private static int SPLASH_TIME_OUT = 3000;
	private DBHelper helper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		helper = new DBHelper(getApplicationContext());
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_splash);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				Intent i = new Intent(getApplicationContext(),
						DashboardActivity.class);
				startActivity(i);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

				finish();
			}
		}, SPLASH_TIME_OUT);

	}

}
