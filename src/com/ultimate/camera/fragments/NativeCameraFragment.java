/*
 * Copyright (c) 2014 Rex St. John on behalf of AirPair.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.ultimate.camera.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.raay.jagajantung.R;
import com.raay.jagajantung.fragments.BaseContainerFragment;
import com.raay.jagajantung.fragments.BaseFragment;
import com.raay.jagajantung.fragments.TNCFragment;

/**
 * Take a picture directly from inside the app using this fragment.
 * 
 * Reference: http://developer.android.com/training/camera/cameradirect.html
 * Reference:
 * http://stackoverflow.com/questions/7942378/android-camera-will-not-
 * work-startpreview-fails Reference:
 * http://stackoverflow.com/questions/10913181/camera-preview-is-not-restarting
 * 
 * Created by Rex St. John (on behalf of AirPair.com) on 3/4/14.
 */
public class NativeCameraFragment extends BaseFragment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@InjectView(R.id.tv_action)
	TextView tv_action;

	@InjectView(R.id.tv_guide)
	TextView tv_guide;

	@InjectView(R.id.btn_logout)
	Button btn_tnc;

	@InjectView(R.id.text_heart_rate)
	TextView tv_rate;

	private Context context;

	/**
	 * Default empty constructor.
	 */
	public NativeCameraFragment() {
		super();

	}

	/**
	 * OnCreateView fragment override
	 * 
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.activity_progress_front,
				container, false);

		ButterKnife.inject(this, view);

		context = getActivity();

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);

		btn_tnc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				((BaseContainerFragment) getParentFragment()).replaceFragment(
						new TNCFragment(), true, true);

			}
		});

		ImageView img_begin = (ImageView) view.findViewById(R.id.img_begin);
		img_begin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((BaseContainerFragment) getParentFragment()).replaceFragment(
						new CameraCardioFragment(), true, false);
			}
		});

		tv_action.setText(context.getString(R.string.start));
		tv_guide.setText(context.getString(R.string.main_guide));

		tv_rate.setVisibility(View.INVISIBLE);

	}

	@Override
	public void onPause() {
		super.onPause();

	}

	@Override
	public void onResume() {
		super.onResume();

	}

}
