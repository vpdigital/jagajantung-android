/*
 * Copyright (c) 2014 Rex St. John on behalf of AirPair.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.ultimate.camera.fragments;

import java.util.concurrent.atomic.AtomicBoolean;

import org.achartengine.GraphicalView;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.jwetherell.heart_rate_monitor.ImageProcessing;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.pnikosis.materialishprogress.ProgressWheel.ProgressCallback;
import com.raay.jagajantung.R;
import com.raay.jagajantung.constant.AppConstant;
import com.raay.jagajantung.constant.DBConstant;
import com.raay.jagajantung.fragments.BaseContainerFragment;
import com.raay.jagajantung.fragments.BaseFragment;
import com.raay.jagajantung.fragments.ResultFragment;
import com.raay.jagajantung.fragments.TNCFragment;
import com.raay.jagajantung.utils.Utils;

/**
 * Take a picture directly from inside the app using this fragment.
 * 
 * Reference: http://developer.android.com/training/camera/cameradirect.html
 * Reference:
 * http://stackoverflow.com/questions/7942378/android-camera-will-not-
 * work-startpreview-fails Reference:
 * http://stackoverflow.com/questions/10913181/camera-preview-is-not-restarting
 * 
 * Created by Rex St. John (on behalf of AirPair.com) on 3/4/14.
 */
public class CameraFragment extends BaseFragment {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	private static final String TAG = "HeartRateMonitor";
	private static final AtomicBoolean processing = new AtomicBoolean(false);

	private static SurfaceView preview = null;
	private static SurfaceHolder mHolder = null;
	private static Camera mCamera = null;
	private static View image = null;
	private static TextView tv_rate = null;

	private static int averageIndex = 0;
	private static final int averageArraySize = 4;
	private static final int[] averageArray = new int[averageArraySize];

	public static enum TYPE {
		GREEN, RED
	};

	private static TYPE currentType = TYPE.GREEN;

	public static TYPE getCurrent() {
		return currentType;
	}

	private static int beatsIndex = 0;
	private static final int beatsArraySize = 3;
	private static final int[] beatsArray = new int[beatsArraySize];
	private static double beats = 0;
	private static long startTime = 0;
	public static GraphicalView graphicalView;

	static int nofdpoints = 0;
	static int count;

	private static WakeLock wakeLock = null;

	@InjectView(R.id.tv_action)
	TextView tv_action;

	@InjectView(R.id.tv_guide)
	TextView tv_guide;

	@InjectView(R.id.btn_logout)
	Button btn_tnc;

	@InjectView(R.id.img_begin)
	ImageView btn_begin;

	private static int heart_result = 0;

	private SharedPreferences shared;
	// Flash modes supported by this camera
	private ProgressWheel progressWheel;
	private static Context context;
	private boolean onResumeSpesialCase = true;

	/**
	 * Default empty constructor.
	 */
	public CameraFragment() {
		super();

	}

	/**
	 * OnCreateView fragment override
	 * 
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.activity_progress,
				container, false);

		ButterKnife.inject(this, view);

		tv_rate = (TextView) view.findViewById(R.id.text_heart_rate);
		preview = (SurfaceView) view.findViewById(R.id.preview);
		progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);

		onResumeSpesialCase = false;

		context = getActivity();

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);

		PowerManager pm = (PowerManager) getActivity().getSystemService(
				Context.POWER_SERVICE);
		wakeLock = pm
				.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");

		mHolder = preview.getHolder();
		mHolder.addCallback(surfaceCallback);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		progressWheel.reSpin();

		onResumeSpesialCase = false;
		tv_action.setText(context.getString(R.string.stop));
		tv_guide.setText(context.getString(R.string.measuring_rate));

		tv_rate.setText(context.getString(R.string.heart_rate_result_initial));

		btn_begin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				((BaseContainerFragment) getParentFragment()).popFragment();
			}
		});

		btn_tnc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				((BaseContainerFragment) getParentFragment()).replaceFragment(
						new TNCFragment(), true, true);

			}
		});

		shared = getActivity().getSharedPreferences(AppConstant.SHARED_KEY,
				Context.MODE_PRIVATE);

		// text.setVisibility(view.INVISIBLE);

		progressWheel.setCallback(new ProgressCallback() {

			@Override
			public void onProgressUpdate(float progress) {
				if (progress == 360.0f) {

					releaseCameraAndPreview();

					progressWheel.stopSpinning();

					// tv_action.setText(context.getString(R.string.start));
					// tv_guide.setText(context.getString(R.string.main_guide));

					Editor edit = shared.edit();
					edit.putString(DBConstant.CREATED_DATE, Utils.getDateTime());
					edit.commit();

					((BaseContainerFragment) getParentFragment())
							.replaceFragment(new ResultFragment(heart_result),
									false, true);
				}

			}
		});

	}

	/**
	 * Safe method for getting a camera instance.
	 * 
	 * @return
	 */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open(); // attempt to get a Camera instance
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c; // returns null if camera is unavailable
	}

	private static PreviewCallback previewCallback = new PreviewCallback() {

		/**
		 * {@inheritDoc}
		 */

		@Override
		public void onPreviewFrame(byte[] data, Camera cam) {

			if (data == null)
				throw new NullPointerException();
			Camera.Size size = cam.getParameters().getPreviewSize();
			if (size == null)
				throw new NullPointerException();

			if (!processing.compareAndSet(false, true))
				return;

			int width = size.width;
			int height = size.height;

			int imgAvg = ImageProcessing.decodeYUV420SPtoRedAvg(data.clone(),
					height, width);
			Log.i(TAG, "imgAvg=" + imgAvg);
			if (imgAvg == 0 || imgAvg == 255) {
				processing.set(false);
				return;
			}

			int averageArrayAvg = 0;
			int averageArrayCnt = 0;
			for (int i = 0; i < averageArray.length; i++) {
				if (averageArray[i] > 0) {
					averageArrayAvg += averageArray[i];
					averageArrayCnt++;
				}
			}

			int rollingAverage = (averageArrayCnt > 0) ? (averageArrayAvg / averageArrayCnt)
					: 0;
			TYPE newType = currentType;
			if (imgAvg < rollingAverage) {
				newType = TYPE.RED;
				if (newType != currentType) {
					beats++;
					Log.d(TAG, "BEAT!! beats=" + beats);
				}
			} else if (imgAvg > rollingAverage) {
				newType = TYPE.GREEN;
			}

			if (averageIndex == averageArraySize)
				averageIndex = 0;
			averageArray[averageIndex] = imgAvg;
			averageIndex++;

			// Transitioned from one state to another to the same
			if (newType != currentType) {
				currentType = newType;
				// image.postInvalidate();
			}

			long endTime = System.currentTimeMillis();
			double totalTimeInSecs = (endTime - startTime) / 1000d;
			if (totalTimeInSecs >= 10) {
				double bps = (beats / totalTimeInSecs);
				int dpm = (int) (bps * 60d);
				if (dpm < 30 || dpm > 180) {
					startTime = System.currentTimeMillis();
					beats = 0;
					processing.set(false);
					return;

				}

				Log.d(TAG, "totalTimeInSecs=" + totalTimeInSecs + " beats="
						+ beats);

				if (beatsIndex == beatsArraySize)
					beatsIndex = 0;
				beatsArray[beatsIndex] = dpm;
				beatsIndex++;

				int beatsArrayAvg = 0;
				int beatsArrayCnt = 0;
				for (int i = 0; i < beatsArray.length; i++) {
					if (beatsArray[i] > 0) {
						beatsArrayAvg += beatsArray[i];
						beatsArrayCnt++;
					}
				}
				int beatsAvg = (beatsArrayAvg / beatsArrayCnt);
				Log.d("DEBUG JAGA", String.valueOf(beatsAvg));
				tv_rate.setText(context.getString(R.string.heart_rate_result,
						String.format("%03d", beatsAvg)));
				heart_result = beatsAvg;
				nofdpoints++;
				count++;

				startTime = System.currentTimeMillis();
				beats = 0;
				TonedGenerator toneGenerator = new TonedGenerator();
				toneGenerator.playTone();
			}
			processing.set(false);
		}

	};

	private static SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			try {
				mCamera.setPreviewDisplay(mHolder);
				mCamera.setPreviewCallback(previewCallback);
			} catch (Throwable t) {
				Log.e("PreviewDemo-surfaceCallback",
						"Exception in setPreviewDisplay()", t);
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			Camera.Parameters parameters = mCamera.getParameters();
			parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
			Camera.Size size = getSmallestPreviewSize(width, height, parameters);
			if (size != null) {
				parameters.setPreviewSize(size.width, size.height);
				Log.d(AppConstant.APP_NAME, "Using width=" + size.width
						+ " height=" + size.height);
			}
			// parameters.set("jpeg-quality", 100);
			parameters.setRotation(90);
			// parameters.setPictureFormat(PixelFormat.JPEG);

			mCamera.setParameters(parameters);
			mCamera.startPreview();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			// Ignore
		}
	};

	private static Camera.Size getSmallestPreviewSize(int width, int height,
			Camera.Parameters parameters) {
		Camera.Size result = null;

		for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
			if (size.width <= width && size.height <= height) {
				if (result == null) {
					result = size;
				} else {
					int resultArea = result.width * result.height;
					int newArea = size.width * size.height;

					if (newArea < resultArea)
						result = size;
				}
			}
		}

		return result;
	}

	private void releaseCameraAndPreview() {
		if (mCamera != null) {
			wakeLock.release();

			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;

			if (preview != null) {
				preview.setVisibility(View.INVISIBLE);
				preview = null;
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		releaseCameraAndPreview();
		onResumeSpesialCase = true;
	}

	@Override
	public void onResume() {
		super.onResume();

		if (onResumeSpesialCase) {

			((BaseContainerFragment) getParentFragment()).replaceFragment(
					new NativeCameraFragment(), false, false);

		} else {
			if (mCamera == null) {
				wakeLock.acquire();

				mCamera = getCameraInstance();

				startTime = System.currentTimeMillis();
			}
		}

	}

	/**
	 * Begin the preview of the camera input.
	 */
	public void startCameraPreview() {

		if (mCamera != null) {
			try {
				mCamera.setPreviewCallback(previewCallback);
				mCamera.setPreviewDisplay(mHolder);
				mCamera.startPreview();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}

class TonedGenerator {
	private final int duration = 1; // seconds
	private final int sampleRate = 8000;
	private final int numSamples = duration * sampleRate;
	private final double sample[] = new double[numSamples];
	private final double freqOfTone = 440; // hz

	private final byte generatedSnd[] = new byte[2 * numSamples];

	Handler handler = new Handler();

	public void playTone() {
		final Thread thread = new Thread(new Runnable() {
			public void run() {
				genTone();
				handler.post(new Runnable() {

					public void run() {
						playSound();
					}
				});
			}
		});
		thread.start();
	}

	void genTone() {
		// fill out the array
		for (int i = 0; i < numSamples; ++i) {
			sample[i] = Math.sin(2 * Math.PI * i / (sampleRate / freqOfTone));
		}

		// convert to 16 bit pcm sound array
		// assumes the sample buffer is normalised.
		int idx = 0;
		for (final double dVal : sample) {
			// scale to maximum amplitude
			final short val = (short) ((dVal * 32767));
			// in 16 bit wav PCM, first byte is the low order byte
			generatedSnd[idx++] = (byte) (val & 0x00ff);
			generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);

		}
	}

	void playSound() {
		final AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
				sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
				AudioFormat.ENCODING_PCM_16BIT, numSamples,
				AudioTrack.MODE_STATIC);
		audioTrack.write(generatedSnd, 0, generatedSnd.length);
		audioTrack.play();
	}
}